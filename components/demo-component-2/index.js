import React from 'react';
import refineData from '@demo/function';

export default ({ input }) => (
  <div>{refineData(input)}</div>
)