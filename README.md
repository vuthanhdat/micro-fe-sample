# Micro Front-end Demo

a simple approach of micro front-end here

## Getting Started

Install dependencies first

```
npm install
```

### Installing - Frontend

First of all, install dependencies of all sub project by this command in the root of project
```
lerna bootstrap
```

And build for production to serve

```
lerna run build
```

And start the project
```
lerna run serve
```

### Installing - Backend

from /server/graphql_services/
Install dependencies first

```
npm install
```

```
npm install nodemon -g 
```

```
nodemon server3.js 
```

back-end will be serve at port 4000, for API Document you can access via http://localhost:4000/graphql

main project will run at port 3000, and sub project be hosted from 3001 -> 3005

End with an example of getting some data out of the system or using it for a little demo

## Built With

* [ReactJS](https://reactjs.org/docs/) - The front-end library built by JS developed by Facebook
* [Angular](https://angular.io/docs) - The UI framework of google.
* [Single-SPA](https://single-spa.js.org/) - Used to implement Micro front-end 

## Authors

* **Dat Ta** - *Initial work* - [GitLab](https://gitlab.com/vuthanhdat/micro-fe)

## Acknowledgments

* 
* Inspiration
* etc
