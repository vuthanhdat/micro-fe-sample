var express = require('express');
var graphqlHTTP = require('express-graphql');

const mongoose = require('mongoose');
const cors = require('cors');
const schema = require('./schema/schema');

// connect to local database 
mongoose.connect('mongodb://localhost:27017/demo_graphql', { useNewUrlParser: true });
mongoose.connection.once('open', () => {
    console.log('connected to mongodb')
});

var app = express();
app.use(cors());
app.use('/graphql', graphqlHTTP({schema: schema,graphiql: true}));


app.listen(4000, () => console.log('Express GQL is Running'));
