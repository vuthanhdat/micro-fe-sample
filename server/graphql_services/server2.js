var express = require('express');
var express_graphql = require('express-graphql');
var { buildSchema } = require('graphql');

var schema = buildSchema(`
    type Query {
        message: String
        test(id: Int!): Test
        tests(topic: String): [Test]
    }
    type Mutation {
        updateTestTopic(id: Int!, topic: String!): Test
    }
    type Test {
        id: Int
        title: String
        url: String
        topic: String
    }
`);

var testData = [
    {
        id: 1,
        title: '123451524213423141234',
        url: 'codernator.vn',
        topic: '123'
    },
    {
        id: 2,
        title: ':v',
        url: 'grab.vn',
        topic: ':)'
    },
    {
        id: 3,
        title: ':3',
        url: 'wizeline',
        topic: ':('
    },
]

var getTest = (args) => {
    console.log(args);
    var id = args.id;
    return testData.filter(test => {
        return test.id === id;
    })[0];
}

var getAllTest = (args) => {
    if (args.topic) {
        var topic = args.topic;
        return testData.filter(item => {
            return item.topic === topic
        })
    } else {
        return testData;
    }
}

var updateTestTopic = ({id, topic}) => {
    testData.map(item=> {
        item.id === id ? item.topic = topic : item;
        return item;
    })
    return testData.filter(item=> item.id === id)[0];
}

var root = {
    message: () => 'test graphql working',
    test: getTest,
    tests: getAllTest,
    updateTestTopic: updateTestTopic
};

var app = express();
app.use('/graphql', express_graphql({
    schema: schema,
    rootValue: root,
    graphiql: true,
}));

app.listen(4000, () => console.log('Express GQL is Running'))