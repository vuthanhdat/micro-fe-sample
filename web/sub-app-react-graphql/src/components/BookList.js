import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { getBooksQuery } from '../queries/queries';

// components
import BookDetails from './BookDetails';

class BookList extends Component {
    constructor(props){
        super(props);
        this.state = {
            selected: null
        }
    }

    displayBooks(){
        const {data} = this.props;
        const { loading, books } = data;
        if(loading){
            return( <div>Loading books...</div> );
        } else {
            return books ? books.map(book => {
                return(
                    <li key={ book.id } onClick={ (e) => this.setState({ selected: book.id }) }>{ book.name }</li>
                );
            }) : ""
        }
    }

    render(){
        const { selected } = this.state;
        return(
            <div>
                <ul id="book-list">
                    { this.displayBooks() }
                </ul>
                <BookDetails bookId={ selected } />
            </div>
        );
    }
}

export default graphql(getBooksQuery)(BookList);
