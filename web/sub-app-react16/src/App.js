import React, { Component } from 'react';
import './App.css';
import { Clock } from "./Clock";
import TestComponent from '@micro-fe/test'
//import './styles/bootstrap/bootstrap.min.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div>{TestComponent}</div>
        <Clock/>
      </div>
    );
  }
}

export default App;
