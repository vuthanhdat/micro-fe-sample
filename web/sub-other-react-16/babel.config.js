require('@babel/register')({
  ignore: [filepath => {
    return filepath.includes('node_modules') && !filepath.includes('@demo');
  }],
});

module.exports = {
  presets: ['@babel/preset-env', '@babel/preset-react']
};