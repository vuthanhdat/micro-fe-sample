import React from 'react';
import Component1 from '@demo/component-1';
import Component2 from '@demo/component-2';
import logo from './logo.svg';
import './App.css';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <Component1 input="input" />
          <Component2 input="input" />
        </header>
      </div>
    );
  }
}

export default App;
