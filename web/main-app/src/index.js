import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as singleSpa from 'single-spa';
import {registerReactApp} from "./apps/react-app";
//import {registerReactOtherApp} from "./apps/react-other-app";
import {registerAngularApp} from "./apps/angular-app";
import {registerReactGraphQLApp} from "./apps/react-graphql-app";


ReactDOM.render(<App/>, document.getElementById('root'));


registerReactApp();
//registerReactOtherApp();
registerAngularApp();
registerReactGraphQLApp();

singleSpa.start();
