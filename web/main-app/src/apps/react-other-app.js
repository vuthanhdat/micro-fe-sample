import * as singleSpa from "single-spa";
import {matchingPathname, runScript} from "./utils";

const loadReactOtherApp = async () => {
    await runScript('http://localhost:3003/static/js/main.js');
    return window.reactOtherApp;
};


export const registerReactOtherApp = () => {
    singleSpa.registerApplication('react-other-app', loadReactOtherApp, matchingPathname(['/react-other', '/']));
};