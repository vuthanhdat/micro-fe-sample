import * as singleSpa from "single-spa";
import {matchingPathname, runScript} from "./utils";

const loadReactApp = async () => {
    await runScript('http://localhost:3004/static/js/main.js');
    return window.reactGraphQLApp;
};

export const registerReactGraphQLApp = () => {
    singleSpa.registerApplication('react-graphql', loadReactApp, matchingPathname(['/react-graphql', '/']));
};